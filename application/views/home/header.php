<div class="wrapper-body">
	<div id="header">
		<nav id="nav" class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="<?= site_url() ?>">
						<h2>BUPA</h2>
					</a>
				</div>
				<div class="float-right">
					<span>Data Uploaded</span> <span class="count"><?= $h['user']; ?>
					</span>
				</div>
			</div>
		</nav>
	</div>
</div>