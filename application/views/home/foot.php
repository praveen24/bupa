		<!--SECTION required query-->
		<!--===============================================================-->

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			var base_url = '<?= base_url() ?>';
			var site_url = '<?= site_url() ?>';
		</script>
		<script src="<?= base_url('assets/js/scripts.js') ?>"></script>
		<script type="text/javascript">
			$(document).on('click', '.add_new', function(){

				var len = $('.driver_info').length;

				var form = `<div class="row driver_info">
								<div class="col-12">
									<div class="container">
										<div class="row">
											<div class="col-12">
												<div class="remove_form">
													<i class="fa fa-close"></i>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<label>Driver Name</label>
											</div>
											<div class="col-8">
												<div class="form-group">
													<input type="text" name="driver_name[]" class="form-control">
												</div>
											</div>
											<div class="col-2">
												<img src="<?= base_url('assets/images/insurance-icon.png') ?>" class="d-block m-auto">
											</div>
											<div class="col-2">
												<div class="checkbox">
													<label class="container_check">
														<input type="checkbox" value="1" name="is_insured[${len}]">
														<span class="checkmark"></span>
													</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<label>Mobile Number</label>
											</div>
											<div class="col-8">
												<div class="form-group">
													<input type="text" name="mobile_number[]" class="form-control">
												</div>
											</div>
											<div class="col-2">
												<img src="<?= base_url('assets/images/whatsapp.png') ?>" class="d-block m-auto">
											</div>
											<div class="col-2">
												<div class="checkbox">
													<label class="container_check">
														<input type="checkbox" value="1" name="is_whatsapp[${len}]">
														<span class="checkmark"></span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>`;

				$('.new_form').append(form);
				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
			})
			$(document).on('click', '.remove_form', function(){
				$(this).closest('.driver_info').remove();
			})
		</script>
	</body>
</html>