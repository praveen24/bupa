<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        table{
            width: 90%;
            border-collapse: collapse;
            margin: 0 auto;
        }
        table td,
        table th{
            padding: 10px;
            background-color: #fcfcfc;
            border: 1px solid #d8d8d8;
            text-align: left;
            word-break: break-all;
            word-wrap: break-word;
        }

        div.wrapper{
            background:#f5f5f5;
            border-radius: 5px;
            padding: 1%;
        }

        h2{
            text-transform: uppercase;
            text-decoration: underline;
        }

    </style>
</head>
<body>

    <div class="wrapper">
        <table style="border: 1px solid #ddd;width: 100%;border-collapse: collapse;">
            <tr>
                <td align="center" style="width: 100%" colspan="2">
                    <img src="<?= base_url('assets/images/englogo.png') ?>" style="margin-right: auto;margin-left:auto;display: block;width: 150px;text-align: center;  ">
                    <h2 style="text-align: center"> Registration Details </h2>
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Name of College</th>
                <td style="width:60%;padding: 10px"><?= $college ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Name of Student</th>
                <td style="width:60%;padding: 10px"><?= $student ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Date of Birth</th>
                <td style="width:60%;padding: 10px"><?= $dob ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Age</th>
                <td style="width:60%;padding: 10px"><?= $age ?></td>
            </tr>
            <tr>
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Gender</th>
                <td style="width:60%;padding: 10px"><?= $gender ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Name of Father</th>
                <td style="width:60%;padding: 10px"><?= $father ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Name of Mother</th>
                <td style="width:60%;padding: 10px"><?= $mother ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Father's Occupation</th>
                <td style="width:60%;padding: 10px"><?= $father_occupation ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Mother's Occupation</th>
                <td style="width:60%;padding: 10px"><?= $mother_occupation ?></td>
            </tr>
            <tr>
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Addres for Communication</th>
                <td style="width:60%;padding: 10px"><?= $address ?></td>
            </tr>

            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Course Looking for</th>
                <td style="width:60%;padding: 10px"><?= $course ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Reason for Joining the Course</th>
                <td style="width:60%;padding: 10px"><?= $reason ?></td>
            </tr>
            <tr>
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Date & Year of Passing SSLC</th>
                <td style="width:60%;padding: 10px"><?= $sslc_pass ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">% of Mark in SSLC</th>
                <td style="width:60%;padding: 10px"><?= $sslc_mark ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">% of Mark in Plus One</th>
                <td style="width:60%;padding: 10px"><?= $plusone_mark ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">% of Mark in Plus Two</th>
                <td style="width:60%;padding: 10px"><?= $plustwo_mark ?></td>
            </tr>
            <tr style="border-bottom: 1px solid #ddd;">
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Phone Number of Student</th>
                <td style="width:60%;padding: 10px"><?= $student_phone ?></td>
            </tr>
            <tr>
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Phone Number of Parent</th>
                <td style="width:60%;padding: 10px"><?= $parent_phone ?></td>
            </tr>
            <tr>
                <th style="width:40%;padding: 10px;border-right: 1px solid #ddd">Signature of the Student</th>
                <td style="width:60%;padding: 10px"><?= $student_sign ?></td>
            </tr>
        </table>
    </div>
    
</body>
</html>