
<div class="container-fluid">
    <form action="" method="post">
        <div class="row info_section">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata('success')) : ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $this->session->flashdata('success'); ?>
                                </div>
                            <?php elseif ($this->session->flashdata('error')) : ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $this->session->flashdata('error'); ?>
                                </div>
                            <?php elseif ($this->session->flashdata('info')) : ?>
                                <div class="alert alert-info alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $this->session->flashdata('info'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-3"></div>
                        <div class="col-9 form-group">
                            <div class="checkbox float-left">
                                <label class="container_check">
                                    <input type="radio" name="category" <?=set_value('category') == 'Muncipality' ? 'checked':'' ?> value="Muncipality"> Muncipality
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="checkbox float-left">
                                <label class="container_check">
                                    <input type="radio" name="category" value="Panchayat" <?=set_value('category') == 'Panchayat' ? 'checked':'' ?>> Panchayat
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="checkbox float-left">
                                <label class="container_check">
                                    <input type="radio" name="category" value="Corporation" <?=set_value('category') == 'Corporation' ? 'checked':'' ?>> Corporation
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label>City</label>
                        </div>
                        <div class="col-9 form-group">
                            <input type="text" name="city" value="<?=set_value('city') ?>" class="form-control <?=form_error('city') ? 'is-invalid' : ''?>" required="">
                            <span class="invalid-feedback"><?=form_error('city')?></span>
                            <span>*</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label>Stand</label>
                        </div>
                        <div class="col-9 form-group">
                            <input type="text" name="panchayat" value="<?=set_value('panchayat') ?>" class="form-control" required="">
                            <span>*</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <label>District</label>
                        </div>
                        <div class="col-9 form-group">
                            <select name="district" class="form-control" required="">
                                <option <?= set_value('district') == '' ? 'selected' : '' ?> disabled="">Select District</option>
                                <option <?= set_value('district') == 'Kasaragod' ? 'selected' : '' ?> value="Kasaragod">Kasaragod</option>
                                <option <?= set_value('district') == 'Kannur' ? 'selected' : '' ?> value="Kannur">Kannur</option>
                                <option <?= set_value('district') == 'Wayanad' ? 'selected' : '' ?> value="Wayanad">Wayanad</option>
                                <option <?= set_value('district') == 'Kozhikode' ? 'selected' : '' ?> value="Kozhikode">Kozhikode</option>
                                <option <?= set_value('district') == 'Malappuram' ? 'selected' : '' ?> value="Malappuram">Malappuram</option>
                                <option <?= set_value('district') == 'Palakkad' ? 'selected' : '' ?> value="Palakkad">Palakkad</option>
                                <option <?= set_value('district') == 'Thrissur' ? 'selected' : '' ?> value="Thrissur">Thrissur</option>
                                <option <?= set_value('district') == 'Ernakulam' ? 'selected' : '' ?> value="Ernakulam">Ernakulam</option>
                                <option <?= set_value('district') == 'Idukki' ? 'selected' : '' ?> value="Idukki">Idukki</option>
                                <option <?= set_value('district') == 'Kottayam' ? 'selected' : '' ?> value="Kottayam">Kottayam</option>
                                <option <?= set_value('district') == 'Alappuzha' ? 'selected' : '' ?> value="Alappuzha">Alappuzha</option>
                                <option <?= set_value('district') == 'Pathanamthitta' ? 'selected' : '' ?> value="Pathanamthitta">Pathanamthitta</option>
                                <option <?= set_value('district') == 'Kollam' ? 'selected' : '' ?> value="Kollam">Kollam</option>
                                <option <?= set_value('district') == 'Trivandrum' ? 'selected' : '' ?> value="Trivandrum">Trivandrum</option>
                            </select>
                            <span>*</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row driver_info">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <label>Driver Name</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="driver_name1" value="<?=set_value('driver_name1') ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/insurance-icon.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" <?=set_value('is_insured1') ? 'checked' : '' ?> name="is_insured1">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Mobile Number</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="mobile_number1" value="<?=set_value('mobile_number1') ?>"  class="form-control <?= form_error('mobile_number1') ? 'is-invalid' : '' ?>" maxlength="10" minlength="10" pattern="^\d{10}$">
                                <span class="invalid-feedback"><?= form_error('mobile_number1') ?></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/whatsapp.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" <?=set_value('is_whatsapp1') ? 'checked' : '' ?> name="is_whatsapp1">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row driver_info">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <label>Driver Name</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="driver_name2" class="form-control" value="<?=set_value('driver_name2') ?>">
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/insurance-icon.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" name="is_insured2">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Mobile Number</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="mobile_number2" class="form-control <?= form_error('mobile_number2') ? 'is-invalid' : '' ?>" value="<?=set_value('mobile_number2') ?>" maxlength="10" minlength="10" pattern="^\d{10}$">
                                <span class="invalid-feedback"><?= form_error('mobile_number2') ?></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/whatsapp.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" name="is_whatsapp2">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row driver_info">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <label>Driver Name</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="driver_name3" class="form-control" value="<?=set_value('driver_name3') ?>">
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/insurance-icon.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" name="is_insured3">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Mobile Number</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="mobile_number3" class="form-control <?= form_error('mobile_number3') ? 'is-invalid' : '' ?> " maxlength="10" minlength="10" pattern="^\d{10}$" value="<?=set_value('mobile_number3') ?>">
                                <span class="invalid-feedback"><?= form_error('mobile_number3') ?></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/whatsapp.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" name="is_whatsapp3">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row driver_info">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <label>Driver Name</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="driver_name4" class="form-control" value="<?=set_value('driver_name4') ?>">
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/insurance-icon.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" name="is_insured4">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label>Mobile Number</label>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <input type="text" name="mobile_number4" class="form-control <?= form_error('mobile_number4') ? 'is-invalid' : '' ?>" maxlength="10" minlength="10" pattern="^\d{10}$" value="<?=set_value('mobile_number4') ?>">
                                <span class="invalid-feedback"><?= form_error('mobile_number4') ?></span>
                            </div>
                        </div>
                        <div class="col-2">
                            <img src="<?= base_url('assets/images/whatsapp.png') ?>" class="d-block m-auto">
                        </div>
                        <div class="col-2">
                            <div class="checkbox">
                                <label class="container_check">
                                    <input type="checkbox" value="1" name="is_whatsapp4">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row btn_section">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-7">
                            <label><b>Stand Group</b></label>
                            <img src="<?= base_url('assets/images/whatsapp.png') ?>">
                            <div class="checkbox float-right">
                                <label class="container_check">
                                    <input type="checkbox" value="1" name="stand_group">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-5">
                            <button class="upload_btn btn-block btn">UPLOAD</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>