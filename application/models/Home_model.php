<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin model
 *
 * @author Fasil K
 */
class Home_model extends MY_Model {

	//put your code here
	public function __construct($table_name = NULL, $primary_key = NULL) {
		parent::__construct($table_name, $primary_key);
	}

	function driver_count() {
	    $query = $this->db->query("SELECT COUNT(*) as user from users");
	    return $query->row_array();
	}

    public function checkMobileNumber($number = null)
    {

        if (is_null($number))
            return false;

        return $this->db->group_start()
            ->where('mobile_number1', $number)
            ->where('mobile_number1 <>', '')
            ->group_end()
            ->or_group_start()
            ->where('mobile_number2', $number)
            ->where('mobile_number2 <>', '')
            ->group_end()
            ->or_group_start()
            ->where('mobile_number3', $number)
            ->where('mobile_number3 <>', '')
            ->group_end()
            ->or_group_start()
            ->where('mobile_number4', $number)
            ->where('mobile_number4 <>', '')
            ->group_end()
            ->get('users')
            ->row();
	}
}

?>