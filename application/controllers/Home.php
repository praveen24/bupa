<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
    }

    public function index()
    {
        $this->user_details();
        $data['h'] = $this->home_model->driver_count();
        $data['data'] = $this->data;
        $data['view_page'] = 'home/index';
        $this->load->view('home/template', $data);
    }

    public function user_details()
    {
        if ($this->input->post()) {

            $this->form_validation->set_rules('driver_name1', 'Driver name', 'trim');
            $this->form_validation->set_rules('mobile_number1', 'Mobile Number', 'trim|callback_validate_unique_number');
            $this->form_validation->set_rules('driver_name2', 'Driver name', 'trim');
            $this->form_validation->set_rules('mobile_number2', 'Mobile Number', 'trim|callback_validate_unique_number');
            $this->form_validation->set_rules('driver_name3', 'Driver name', 'trim');
            $this->form_validation->set_rules('mobile_number3', 'Mobile Number', 'trim|callback_validate_unique_number');
            $this->form_validation->set_rules('driver_name4', 'Driver name', 'trim');
            $this->form_validation->set_rules('mobile_number4', 'Mobile Number', 'trim|callback_validate_unique_number');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('panchayat', 'Panchayat', 'trim|required');
            $this->form_validation->set_rules('district', 'District', 'trim|required');
            $this->form_validation->set_rules('category', 'Category', 'trim|required');
            $this->form_validation->set_rules('is_insured1', 'Is Insured', 'trim');
            $this->form_validation->set_rules('is_whatsapp1', 'Is Whatsapp', 'trim');
            $this->form_validation->set_rules('is_insured2', 'Is Insured', 'trim');
            $this->form_validation->set_rules('is_whatsapp2', 'Is Whatsapp', 'trim');
            $this->form_validation->set_rules('is_insured3', 'Is Insured', 'trim');
            $this->form_validation->set_rules('is_whatsapp3', 'Is Whatsapp', 'trim');
            $this->form_validation->set_rules('is_insured4', 'Is Insured', 'trim');
            $this->form_validation->set_rules('is_whatsapp4', 'Is Whatsapp', 'trim');
            $this->form_validation->set_rules('stand_group', 'Stand Group', 'trim');


            if ($this->form_validation->run() != false) {
                $post = $this->input->post();

                $data = array(
                    'city' => $this->input->post('city'),
                    'panchayat' => $this->input->post('panchayat'),
                    'district' => $this->input->post('district'),
                    'category' => $this->input->post('category'),
                    'driver_name1' => $this->input->post('driver_name1'),
                    'driver_name2' => $this->input->post('driver_name2'),
                    'driver_name3' => $this->input->post('driver_name3'),
                    'driver_name4' => $this->input->post('driver_name4'),
                    'mobile_number1' => $this->input->post('mobile_number1'),
                    'mobile_number2' => $this->input->post('mobile_number2'),
                    'mobile_number3' => $this->input->post('mobile_number3'),
                    'mobile_number4' => $this->input->post('mobile_number4'),
                    'is_insured1' => $this->input->post('is_insured1') ? $this->input->post('is_insured1') : 0,
                    'is_insured2' => $this->input->post('is_insured2') ? $this->input->post('is_insured2') : 0,
                    'is_insured3' => $this->input->post('is_insured3') ? $this->input->post('is_insured3') : 0,
                    'is_insured4' => $this->input->post('is_insured4') ? $this->input->post('is_insured4') : 0,
                    'is_whatsapp1' => $this->input->post('is_whatsapp1') ? $this->input->post('is_whatsapp1') : 0,
                    'is_whatsapp2' => $this->input->post('is_whatsapp2') ? $this->input->post('is_whatsapp2') : 0,
                    'is_whatsapp3' => $this->input->post('is_whatsapp3') ? $this->input->post('is_whatsapp3') : 0,
                    'is_whatsapp4' => $this->input->post('is_whatsapp4') ? $this->input->post('is_whatsapp4') : 0,
                    'stand_group' => $this->input->post('stand_group') ? $this->input->post('stand_group') : 0
                );
                $users = $this->home_model->insertRow($data, 'users');
                $this->session->set_flashdata('success', 'Successfully added the driver details');
                redirect(site_url());
            }
        }
    }

    public function validate_unique_number($str)
    {
        $record = $this->home_model->checkMobileNumber($str);
        if ($record) {
            $this->form_validation->set_message('validate_unique_number', 'The %s is already used by someone else.');
            return false;
        } else {
            return true;
        }
    }

}
