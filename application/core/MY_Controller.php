<?php

/**
 *
 */
class MY_Controller extends CI_Controller {

	public $data;

	function __construct() {
		parent::__construct();
		$this->data = array();
		$this->load->library('pagination');
		$this->load->model('home_model');
	}

	public function response($data = array()){
		header("Content-type:application/json");
		die(json_encode($data));
	}

	function set_upload_options($path, $file_type='*') {
		// upload an image options
		$config = array();
		$config['upload_path'] = $path; //give the path to upload the image in folder
		$config['remove_spaces'] = TRUE;
		$config['encrypt_name'] = TRUE; // for encrypting the name
		$config['allowed_types'] = $file_type;

		$config['max_size'] = '78000';
		$config['overwrite'] = FALSE;
		return $config;
	}

}
